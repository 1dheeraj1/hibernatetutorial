package com.dheeraj.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class TestJdbc {

	public static void main(String[] args) 
	{
		String jdbcUrl="jdbc:postgresql://localhost:5432/hb_student_tracker";
		String user="root";
		String pass="root";
		try 
		{
			System.out.println("connecting to database");
			Connection con=DriverManager.getConnection(jdbcUrl, user, pass);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
