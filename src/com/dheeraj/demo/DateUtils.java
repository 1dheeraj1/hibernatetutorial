package com.dheeraj.demo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils 
{
	private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy,mm:dd");
	
	public static Date parseDate(String dateStr) throws Exception
	{
		Date theDate=null;
		if(dateStr!=null)
		{
			theDate=formatter.parse(dateStr);
		}
		return theDate;
	}
	public static String formatDate(Date theDate) throws Exception
	{
		String dateStr=null;
		if(theDate!=null)
		{
			dateStr=formatter.format(theDate);
		}
		return dateStr;
	}
}
