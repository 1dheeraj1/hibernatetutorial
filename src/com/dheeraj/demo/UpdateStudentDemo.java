package com.dheeraj.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.dheeraj.entity.Student;

public class UpdateStudentDemo 
{
	public static void main(String args[])
	{
		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Student.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		Student st=session.get(Student.class, 1);
		st.setEmail("foo@foo.com");
		session.createQuery("update Student s set email='foo@gmail.com' where s.lastName='kumar'").executeUpdate();
		session.getTransaction().commit();
	}
	
}
