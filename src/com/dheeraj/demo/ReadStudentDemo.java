package com.dheeraj.demo;

import java.util.Collections;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.dheeraj.entity.Student;

public class ReadStudentDemo {

	public static void main(String[] args) 
	{	
		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Student.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		try
		{
			session.beginTransaction();
			List<Student> list=session.createQuery("from Student s where s.lastName='sharma'").getResultList();
			for(Student data:list)
			{
				System.out.printf("%d %s %s %s\n",data.getId(),data.getFirstName(),data.getLastName(),data.getEmail());
			}
		}
		finally
		{
			factory.close();
		}
	}

}
