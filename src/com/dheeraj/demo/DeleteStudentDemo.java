package com.dheeraj.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.dheeraj.entity.Student;

public class DeleteStudentDemo {

	public static void main(String[] args) 
	{

		SessionFactory factory=new Configuration().configure().addAnnotatedClass(Student.class).buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		//Student st=session.get(Student.class, 1);
		//session.delete(st);
		session.createQuery("delete from Student where id=2").executeUpdate();
		session.getTransaction().commit();
	}

}
