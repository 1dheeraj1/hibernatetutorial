package com.dheeraj.demo;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.dheeraj.entity.Student;

public class CreateStudentDemo {

	public static void main(String[] args) throws Exception 
	{
		SessionFactory factory=new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
		
		Session session=factory.getCurrentSession();
		
		
		try
		{
			String dateStr="2010,5:11";
			Date date=DateUtils.parseDate(dateStr);
			Student st=new Student("dipika","sharma","dpka@gmail.com",date);
			session.beginTransaction();
			session.save(st);
			session.getTransaction().commit();
			System.out.println("done");
		}
		finally
		{
			factory.close();
		}
	}

}
